package com.example.tel_c.sundawenang.api;

import com.example.tel_c.sundawenang.model.AdminResponseModel;
import com.example.tel_c.sundawenang.model.KabarDesaModel;
import com.example.tel_c.sundawenang.model.KabarDesaResponseModel;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;
import com.example.tel_c.sundawenang.model.RtResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by TEL-C on 5/15/17.
 */

public interface ApiRequest {

    @FormUrlEncoded
    @POST("insert_rpjm.php")
    Call<RpjmResponseModel> sendRpjm(@Field("rpjm_nama") String rpjm_nama,
                                     @Field("rpjm_ket") String rpjm_ket,
                                     @Field("rpjm_jmlwarga") String rpjm_jmlwarga,
                                     @Field("rpjm_biaya") String rpjm_biaya,
                                     @Field("rpjm_lamapemb") String rpjm_lamapemb,
                                     @Field("rpjm_bobot") String rpjm_bobot,
                                     @Field("admin_id") String admin_id);

    @GET("read_rpjm.php")
    Call<RpjmResponseModel> getRpjm();

    @FormUrlEncoded
    @POST("update_rpjm.php")
    Call<RpjmResponseModel> updateRpjm(@Field("rpjm_id") String rpjm_id,
                                       @Field("rpjm_nama") String rpjm_nama,
                                       @Field("rpjm_ket") String rpjm_ket,
                                       @Field("rpjm_jmlwarga") String rpjm_jmlwarga,
                                       @Field("rpjm_biaya") String rpjm_biaya,
                                       @Field("rpjm_lamapemb") String rpjm_lamapemb,
                                       @Field("rpjm_bobot") String rpjm_bobot,
                                       @Field("admin_id") String admin_id);

    @FormUrlEncoded
    @POST("delete_rpjm.php")
    Call<RpjmResponseModel> deleteRpjm(@Field("rpjm_id") String rpjm_id);

    @FormUrlEncoded
    @POST("login.php")
    Call<AdminResponseModel> loginAdmin(@Field("admin_usnm") String admin_usnm,
                                        @Field("admin_pswd") String admin_pswd);


    @FormUrlEncoded
    @POST("regis.php")
    Call<AdminResponseModel> regisAdmin(@Field("admin_nama") String admin_nama,
                                        @Field("admin_usnm") String admin_usnm,
                                        @Field("admin_pswd") String admin_pswd,
                                        @Field("rw_id") String rw_id,
                                        @Field("rt_id") String rt_id);

    @GET("read_kabardesa.php")
    Call<KabarDesaResponseModel> getKabarDesa();

    @GET("read_rt.php")
    Call<RtResponseModel> getRt();
}
