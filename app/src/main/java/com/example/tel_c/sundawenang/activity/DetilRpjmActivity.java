package com.example.tel_c.sundawenang.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.tel_c.sundawenang.R;

public class DetilRpjmActivity extends AppCompatActivity {

    String rpjm_nama, rpjm_ket, rpjm_jmlwarga, rpjm_biaya, rpjm_lamapemb;
    TextView txNama, txKet, txJmlWarga, txBiaya, txLamaPemb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_rpjm);

        Intent dataRpjm = getIntent();
        rpjm_nama = dataRpjm.getStringExtra("rpjm_nama");
        rpjm_ket = dataRpjm.getStringExtra("rpjm_ket");
        rpjm_jmlwarga = dataRpjm.getStringExtra("rpjm_jmlwarga");
        rpjm_biaya = dataRpjm.getStringExtra("rpjm_biaya");
        rpjm_lamapemb = dataRpjm.getStringExtra("rpjm_lamapemb");

        txNama = (TextView) findViewById(R.id.tx_nama_rpjm);
        txKet = (TextView) findViewById(R.id.tx_ket_rpjm);
        txJmlWarga = (TextView) findViewById(R.id.tx_rpjm_jmlwarga);
        txBiaya = (TextView) findViewById(R.id.tx_rpjm_biaya);
        txLamaPemb = (TextView) findViewById(R.id.tx_rpjm_lamapemb);

        txNama.setText(rpjm_nama);
        txKet.setText(rpjm_ket);
        txJmlWarga.setText(rpjm_jmlwarga);
        txBiaya.setText(rpjm_biaya);
        txLamaPemb.setText(rpjm_lamapemb);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
