package com.example.tel_c.sundawenang.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.fragment.ProfilFragment;
import com.example.tel_c.sundawenang.fragment.ViewKabarDesaAdminFragment;
import com.example.tel_c.sundawenang.fragment.ViewRpjmFragment;
import com.example.tel_c.sundawenang.model.AdminModel;

public class AdminActivity extends AppCompatActivity {

    private TextView mTextMessage;
    @Nullable String rt_id, rw_id;
    Intent dataAdmin;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_kabardesa_adm:
                    selectedFragment = ViewKabarDesaAdminFragment.newInstance();
                    break;
                case R.id.navigation_rpjm_adm:
                    selectedFragment = ViewRpjmFragment.newInstance();
                    break;
                case R.id.navigation_profil_adm:
                    selectedFragment = ProfilFragment.newInstance();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_adm, selectedFragment);
            transaction.commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation_adm);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_adm, ViewKabarDesaAdminFragment.newInstance());
        transaction.commit();

        dataAdmin = getIntent();
        Log.d("Rw", dataAdmin.getStringExtra("rw_id"));
        Toast.makeText(AdminActivity.this, "Selamat datang, "
                + dataAdmin.getExtras().get("admin_nama") + "!", Toast.LENGTH_LONG).show();



        Intent kirimRwRt = new Intent(AdminActivity.this, InputRpjmActivity.class);
        kirimRwRt.putExtra("rw_id", rw_id);
        kirimRwRt.putExtra("rt_id", rt_id);
    }

}
