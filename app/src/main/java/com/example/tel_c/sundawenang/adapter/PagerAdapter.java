package com.example.tel_c.sundawenang.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.tel_c.sundawenang.fragment.ViewKabarDesaFragment;
import com.example.tel_c.sundawenang.fragment.ViewSejarahDesaFragment;
import com.example.tel_c.sundawenang.fragment.ViewVisiMisiDesaFragment;

/**
 * Created by TEL-C on 5/16/17.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ViewKabarDesaFragment tab1 = new ViewKabarDesaFragment();
                return tab1;
            case 1:
                ViewVisiMisiDesaFragment tab2 = new ViewVisiMisiDesaFragment();
                return tab2;
            case 2:
                ViewSejarahDesaFragment tab3 = new ViewSejarahDesaFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
