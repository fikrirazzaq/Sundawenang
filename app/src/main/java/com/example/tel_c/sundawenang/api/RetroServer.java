package com.example.tel_c.sundawenang.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by TEL-C on 5/14/17.
 */

public class RetroServer {

    public static final String base_url = "http://juvetic.telclab.com/sundawenang/";

    private static Retrofit retrofit;

    public static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(base_url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public static ApiRequest getRequestService() {
        return getClient().create(ApiRequest.class);
    }
}
