package com.example.tel_c.sundawenang.model;

/**
 * Created by TEL-C on 5/15/17.
 */

public class RtModel {
    String rt_id;
    String rt_nama;
    String rt_kk;
    String rt_kkbyrpjk;
    String rw_id;

    public String getRt_id() {
        return rt_id;
    }

    public void setRt_id(String rt_id) {
        this.rt_id = rt_id;
    }

    public String getRt_nama() {
        return rt_nama;
    }

    public void setRt_nama(String rt_nama) {
        this.rt_nama = rt_nama;
    }

    public String getRt_jmlwarga() {
        return rt_kk;
    }

    public void setRt_jmlwarga(String rt_jmlwarga) {
        this.rt_kk = rt_jmlwarga;
    }

    public String getRt_jmlwargapjk() {
        return rt_kkbyrpjk;
    }

    public void setRt_jmlwargapjk(String rt_jmlwargapjk) {
        this.rt_kkbyrpjk = rt_jmlwargapjk;
    }

    public String getRw_id() {
        return rw_id;
    }

    public void setRw_id(String rw_id) {
        this.rw_id = rw_id;
    }
}
