package com.example.tel_c.sundawenang.model;

import java.util.List;

/**
 * Created by TEL-C on 5/16/17.
 */

public class AdminResponseModel {
    String kode, pesan;
    List<AdminModel> result;

    public List<AdminModel> getResult() {
        return result;
    }

    public void setResult(List<AdminModel> result) {
        this.result = result;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
