package com.example.tel_c.sundawenang.model;

/**
 * Created by TEL-C on 5/15/17.
 */

public class RpjmModel {

    String rpjm_id;
    String rpjm_nama;
    String rpjm_ket;
    String rpjm_jmlwarga;
    String rpjm_biaya;
    String rpjm_lamapemb;
    String rpjm_bobot;
    String admin_id;

    public String getRpjm_bobot() {
        return rpjm_bobot;
    }

    public void setRpjm_bobot(String rpjm_bobot) {
        this.rpjm_bobot = rpjm_bobot;
    }

    public String getRpjm_id() {
        return rpjm_id;
    }

    public void setRpjm_id(String rpjm_id) {
        this.rpjm_id = rpjm_id;
    }

    public String getRpjm_nama() {
        return rpjm_nama;
    }

    public void setRpjm_nama(String rpjm_nama) {
        this.rpjm_nama = rpjm_nama;
    }

    public String getRpjm_ket() {
        return rpjm_ket;
    }

    public void setRpjm_ket(String rpjm_ket) {
        this.rpjm_ket = rpjm_ket;
    }

    public String getRpjm_jmlwarga() {
        return rpjm_jmlwarga;
    }

    public void setRpjm_jmlwarga(String rpjm_jmlwarga) {
        this.rpjm_jmlwarga = rpjm_jmlwarga;
    }

    public String getRpjm_biaya() {
        return rpjm_biaya;
    }

    public void setRpjm_biaya(String rpjm_biaya) {
        this.rpjm_biaya = rpjm_biaya;
    }

    public String getRpjm_lamapemb() {
        return rpjm_lamapemb;
    }

    public void setRpjm_lamapemb(String rpjm_lamapemb) {
        this.rpjm_lamapemb = rpjm_lamapemb;
    }

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }
}
