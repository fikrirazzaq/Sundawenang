package com.example.tel_c.sundawenang.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.AdminActivity;
import com.example.tel_c.sundawenang.activity.MainActivity;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfilFragment extends Fragment {

    TextView txNama;
    Button btnKeluar;

    public static ProfilFragment newInstance() {
        ProfilFragment fragment = new ProfilFragment();
        return fragment;
    }

    public ProfilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profil, container, false);

        btnKeluar = (Button) v.findViewById(R.id.btn_keluar);
        btnKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                getActivity().finish();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:

                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Keluar?").setPositiveButton("Iya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener).show();

            }
        });

        txNama = (TextView) v.findViewById(R.id.tx_nama_admin);
        txNama.setText(getActivity().getIntent().getStringExtra("admin_nama"));
        return v;
    }

}