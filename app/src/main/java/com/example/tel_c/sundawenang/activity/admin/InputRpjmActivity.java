package com.example.tel_c.sundawenang.activity.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputRpjmActivity extends AppCompatActivity {

    EditText edtNamaRpjm, edtKet, edtJmlWarga, edtBiaya, edtLamaPemb;
    Button btnSimpan;
    ImageButton btnMenu;
    ProgressDialog progressDialog;
    String rpjm_id, rpjm_bobot, admin_id, rw_id, rt_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_rpjm);

        edtNamaRpjm = (EditText) findViewById(R.id.edt_nama);
        edtKet = (EditText) findViewById(R.id.edt_ket);
        edtBiaya = (EditText) findViewById(R.id.edt_biaya);
        edtJmlWarga = (EditText) findViewById(R.id.edt_jmlwarga);
        edtLamaPemb = (EditText) findViewById(R.id.edt_lamapemb);

        btnSimpan = (Button) findViewById(R.id.btn_simpan);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        rw_id = intent.getStringExtra("rw_id");
        rt_id = intent.getStringExtra("rt_id");
        admin_id = intent.getStringExtra("admin_id");
        rpjm_id = intent.getStringExtra("rpjm_id");
        if(rpjm_id != null) {
            edtNamaRpjm.setText(intent.getStringExtra("rpjm_nama"));
            edtKet.setText(intent.getStringExtra("rpjm_ket"));
            edtJmlWarga.setText(intent.getStringExtra("rpjm_jmlwarga"));
            edtBiaya.setText(intent.getStringExtra("rpjm_biaya"));
            edtLamaPemb.setText(intent.getStringExtra("rpjm_lamapemb"));
        }

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Menyimpan data...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                String sNama = edtNamaRpjm.getText().toString();
                String sKet = edtKet.getText().toString();
                String sJmlWarga = edtJmlWarga.getText().toString();
                String sBiaya = edtBiaya.getText().toString();
                String sLamaPemb = edtLamaPemb.getText().toString();
                final String sBobot = Integer.toString(bobotRpjm(sJmlWarga, sBiaya, sLamaPemb));
                final String sAdminId = admin_id;

                ApiRequest api = RetroServer.getClient().create(ApiRequest.class);

                Call<RpjmResponseModel> sendRpjm = api.sendRpjm(sNama, sKet, sJmlWarga, sBiaya, sLamaPemb, sBobot, sAdminId);
                sendRpjm.enqueue(new Callback<RpjmResponseModel>() {
                    @Override
                    public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
                        progressDialog.hide();
                        Log.d("Retrofit", "response: " + response.body().toString());
                        String kode = response.body().getKode();
                        Log.d("Bobot", sBobot);


                        if (kode.equals("1")) {
                            Toast.makeText(InputRpjmActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(InputRpjmActivity.this, "Data gagal disimpan", Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
                        progressDialog.hide();
                        Log.d("Retrofit", "Failure: " + "Gagal Mengirim Request" );
                    }
                });
            }
        });

//        btnLihat.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intentLihat = new Intent(InputRpjmActivity.this, ViewRpjmActivity.class);
//                startActivity(intentLihat);
//            }
//        });
//
//        btnUbah.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                progressDialog.setMessage("Mengubah data...");
//                progressDialog.setCancelable(false);
//                progressDialog.show();
//
//                final String sId = rpjm_id;
//                final String sNama = edtNamaRpjm.getText().toString();
//                final String sKet = edtKet.getText().toString();
//                final String sJmlWarga = edtJmlWarga.getText().toString();
//                final String sBiaya = edtBiaya.getText().toString();
//                final String sLamaPemb = edtLamaPemb.getText().toString();
//                final String sAdminId = admin_id;
//
//                ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
//                Call<RpjmResponseModel> updateRpjm = api.updateRpjm(sId, sNama, sKet, sJmlWarga, sBiaya, sLamaPemb, sAdminId);
//                updateRpjm.enqueue(new Callback<RpjmResponseModel>() {
//                    @Override
//                    public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
//                        Log.d("Retrofit", "Response");
//                        Log.d("Data", sId + sNama + sKet + sJmlWarga + sBiaya + sLamaPemb + sAdminId);
//                        Toast.makeText(InputRpjmActivity.this, response.body().getPesan(), Toast.LENGTH_LONG).show();
//                        progressDialog.hide();
//                        edtNamaRpjm.setText("");
//                        edtKet.setText("");
//                        edtBiaya.setText("");
//                        edtJmlWarga.setText("");
//                        edtLamaPemb.setText("");
//
//                        finish();
//                    }
//
//                    @Override
//                    public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
//                        progressDialog.hide();
//                        Log.d("Retrofit", "OnFailure");
//                    }
//                });
//
//            }
//        });
//
//        btnHapus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                progressDialog.setMessage("Menghapus data...");
//                progressDialog.setCancelable(false);
//                progressDialog.show();
//
//                final String sId = rpjm_id;
//
//                ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
//                Call<RpjmResponseModel> deleteRpjm = api.deleteRpjm(sId);
//                deleteRpjm.enqueue(new Callback<RpjmResponseModel>() {
//                    @Override
//                    public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
//                        Log.d("Retrofit", "Response");
//                        Toast.makeText(InputRpjmActivity.this, response.body().getPesan(), Toast.LENGTH_LONG).show();
//                        progressDialog.hide();
//                        Intent intentTerhapus = new Intent(InputRpjmActivity.this, ViewRpjmActivity.class);
//                        startActivity(intentTerhapus);
//                    }
//
//                    @Override
//                    public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
//                        progressDialog.hide();
//                        Log.d("Retrofit", "OnFailure");
//                    }
//                });
//            }
//        });

//        btnMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showMenuPopup(v);
//            }
//        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private int bobotRpjm(String rpjmJmlWarga, String rpjmBiaya, String rpjmLamaPemb) {
        int jmlWarga = Integer.parseInt(rpjmJmlWarga);
        int biaya = Integer.parseInt(rpjmBiaya);
        int lamaPemb = Integer.parseInt(rpjmLamaPemb);

        if (jmlWarga > 2800) {
            return 3;
        } else if (jmlWarga <= 2800) {
            if (biaya <= 250000000) {
                return 2;
            } else if (biaya > 250000000){
                if (lamaPemb < 1) {
                    return 1;
                } else {
                    return 2;
                }
            }
        }
        return 0;
    }
}
