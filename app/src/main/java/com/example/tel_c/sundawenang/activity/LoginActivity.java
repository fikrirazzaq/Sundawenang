package com.example.tel_c.sundawenang.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.AdminModel;
import com.example.tel_c.sundawenang.model.AdminResponseModel;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    EditText edtUsname, edtPswd;
    ProgressDialog progressDialog;
    TextInputLayout inputLayoutUsnm, inputLayoutPswd;

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtUsname = (EditText) findViewById(R.id.edt_usname);
        edtPswd = (EditText) findViewById(R.id.edt_pswd);

        inputLayoutUsnm = (TextInputLayout) findViewById(R.id.input_layout_usname);
        inputLayoutPswd = (TextInputLayout) findViewById(R.id.input_layout_pswd);

        btnLogin = (Button) findViewById(R.id.btn_masuk);

        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Memuat...");

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                ApiRequest api = RetroServer.getRequestService();
                Call<AdminResponseModel> loginAdmin =
                        api.loginAdmin(edtUsname.getText().toString(), edtPswd.getText().toString());
                loginAdmin.enqueue(new Callback<AdminResponseModel>() {
                    @Override
                    public void onResponse(Call<AdminResponseModel> call, Response<AdminResponseModel> response) {
                        progressDialog.dismiss();
                        submitForm();
                        Log.d(TAG, "Response: " + response.toString());
                        AdminResponseModel res = response.body();
                        List<AdminModel> adminModels = res.getResult();
                        if(res.getKode().equals("1")) {

                            if(adminModels.get(0).getRt_id() != null) {
                                finish();
                                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                                intent.putExtra("admin_id", adminModels.get(0).getAdmin_id());
                                intent.putExtra("admin_nama", adminModels.get(0).getAdmin_nama());
                                intent.putExtra("rw_id", adminModels.get(0).getRw_id());
                                intent.putExtra("rt_id", adminModels.get(0).getRt_id());
                                startActivity(intent);

                                Intent kirimData = new Intent(LoginActivity.this, InputRpjmActivity.class);
                                kirimData.putExtra("admin_id", adminModels.get(0).getAdmin_id());
                                kirimData.putExtra("admin_nama", adminModels.get(0).getAdmin_nama());
                                kirimData.putExtra("rw_id", adminModels.get(0).getRw_id());
                                kirimData.putExtra("rt_id", adminModels.get(0).getRt_id());
                            } else {
                                finish();
                                Intent intent = new Intent(LoginActivity.this, SuperAdminActivity.class);
                                intent.putExtra("admin_id", adminModels.get(0).getAdmin_id());
                                intent.putExtra("admin_nama", adminModels.get(0).getAdmin_nama());
                                intent.putExtra("rw_id", adminModels.get(0).getRw_id());
                                intent.putExtra("rt_id", adminModels.get(0).getRt_id());
                                startActivity(intent);

                                Intent kirimData = new Intent(LoginActivity.this, InputRpjmActivity.class);
                                kirimData.putExtra("admin_id", adminModels.get(0).getAdmin_id());
                                kirimData.putExtra("admin_nama", adminModels.get(0).getAdmin_nama());
                                kirimData.putExtra("rw_id", adminModels.get(0).getRw_id());
                                kirimData.putExtra("rt_id", adminModels.get(0).getRt_id());
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, "Username atau password salah!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AdminResponseModel> call, Throwable t) {
                        progressDialog.dismiss();
                        Log.d(TAG, "Error: " + t.getMessage());
                    }
                });
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private boolean validateUsnm() {
        if (edtUsname.getText().toString().trim().isEmpty()) {
            inputLayoutUsnm.setError("Nama pengguna tidak boleh kosong!");
            requestFocus(edtUsname);
            return false;
        } else {
            inputLayoutUsnm.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePswd() {
        if (edtPswd.getText().toString().trim().isEmpty()) {
            inputLayoutPswd.setError("Kata sandi tidak boleh kosong!");
            requestFocus(edtPswd);
            return false;
        } else {
            inputLayoutPswd.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void submitForm() {
        if (!validateUsnm()) {
            return;
        }

        if(!validatePswd()) {
            return;
        }
    }
}