package com.example.tel_c.sundawenang.model;

/**
 * Created by TEL-C on 5/15/17.
 */

public class RwModel {

    String rw_id;
    String rw_nama;

    public String getRw_id() {
        return rw_id;
    }

    public void setRw_id(String rw_id) {
        this.rw_id = rw_id;
    }

    public String getRw_nama() {
        return rw_nama;
    }

    public void setRw_nama(String rw_nama) {
        this.rw_nama = rw_nama;
    }
}
