package com.example.tel_c.sundawenang.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tel_c.sundawenang.activity.DetilRpjmActivity;
import com.example.tel_c.sundawenang.activity.admin.InputRpjmActivity;
import com.example.tel_c.sundawenang.R;
import com.example.tel_c.sundawenang.activity.admin.ViewRpjmActivity;
import com.example.tel_c.sundawenang.api.ApiRequest;
import com.example.tel_c.sundawenang.api.RetroServer;
import com.example.tel_c.sundawenang.model.RpjmModel;
import com.example.tel_c.sundawenang.model.RpjmResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by TEL-C on 5/15/17.
 */

public class AdapterRpjm extends RecyclerView.Adapter<AdapterRpjm.HolderData> {

    private List<RpjmModel> mList;
    private Context mContext;
    private ProgressDialog progressDialog;

    public AdapterRpjm (Context context, List<RpjmModel> list) {
        this.mList = list;
        this.mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rpjm, parent, false);
        HolderData holderData = new HolderData(view);
        return holderData;
    }

    @Override
    public void onBindViewHolder(final HolderData holder, int position) {
        RpjmModel model = mList.get(position);
        holder.txNama.setText(model.getRpjm_nama());
        holder.txKet.setText(model.getRpjm_ket());
        holder.rpjmModel = model;

        progressDialog = new ProgressDialog(mContext);

        holder.btnViewOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(mContext, holder.btnViewOption);
                popupMenu.inflate(R.menu.menu_ubahhapus);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.menu_ubah:
                                Intent intent = new Intent(mContext, InputRpjmActivity.class);
                                intent.putExtra("rpjm_id", holder.rpjmModel.getRpjm_id());
                                intent.putExtra("rpjm_nama", holder.rpjmModel.getRpjm_nama());
                                intent.putExtra("rpjm_ket", holder.rpjmModel.getRpjm_ket());
                                intent.putExtra("rpjm_jmlwarga", holder.rpjmModel.getRpjm_jmlwarga());
                                intent.putExtra("rpjm_biaya", holder.rpjmModel.getRpjm_biaya());
                                intent.putExtra("rpjm_lamapemb", holder.rpjmModel.getRpjm_lamapemb());
                                intent.putExtra("admin_id", holder.rpjmModel.getAdmin_id());

                                int bobot = bobotRpjm(holder.rpjmModel.getRpjm_jmlwarga(), holder.rpjmModel.getRpjm_biaya(),
                                        holder.rpjmModel.getRpjm_lamapemb());
                                intent.putExtra("rpjm_bobot", Integer.toString(bobot));

                                mContext.startActivity(intent);
                                break;
                            case R.id.menu_hapus:
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which){
                                            case DialogInterface.BUTTON_POSITIVE:
                                                final String sId = holder.rpjmModel.getRpjm_id();
                                                progressDialog.setMessage("Mengubah data...");
                                                progressDialog.setCancelable(false);
                                                progressDialog.show();
                                                ApiRequest api = RetroServer.getClient().create(ApiRequest.class);
                                                Call<RpjmResponseModel> deleteRpjm = api.deleteRpjm(sId);
                                                deleteRpjm.enqueue(new Callback<RpjmResponseModel>() {
                                                    @Override
                                                    public void onResponse(Call<RpjmResponseModel> call, Response<RpjmResponseModel> response) {
                                                        Log.d("Retrofit", "Response");
                                                        Toast.makeText(mContext, response.body().getPesan(), Toast.LENGTH_LONG).show();
                                                        progressDialog.hide();
                                                    }

                                                    @Override
                                                    public void onFailure(Call<RpjmResponseModel> call, Throwable t) {
                                                        progressDialog.hide();
                                                        Log.d("Retrofit", "OnFailure");
                                                    }
                                                });
                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:

                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setMessage("Hapus data?").setPositiveButton("Iya", dialogClickListener)
                                        .setNegativeButton("Tidak", dialogClickListener).show();

                                break;
                        }
                        return false;
                    }
                });

                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }



    class HolderData extends RecyclerView.ViewHolder {

        TextView txNama, txKet;
        ImageView btnViewOption;
        RpjmModel rpjmModel;

        public HolderData(View v) {
            super(v);

            txNama = (TextView) v.findViewById(R.id.tx_nama);
            txKet = (TextView) v.findViewById(R.id.tx_ket);
            btnViewOption = (ImageView) v.findViewById(R.id.img_dots);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, DetilRpjmActivity.class);
                    intent.putExtra("rpjm_id", rpjmModel.getRpjm_id());
                    intent.putExtra("rpjm_nama", rpjmModel.getRpjm_nama());
                    intent.putExtra("rpjm_ket", rpjmModel.getRpjm_ket());
                    intent.putExtra("rpjm_jmlwarga", rpjmModel.getRpjm_jmlwarga());
                    intent.putExtra("rpjm_biaya", rpjmModel.getRpjm_biaya());
                    intent.putExtra("rpjm_lamapemb", rpjmModel.getRpjm_lamapemb());
                    intent.putExtra("admin_id", rpjmModel.getAdmin_id());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<RpjmModel> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    private int bobotRpjm(String rpjmJmlWarga, String rpjmBiaya, String rpjmLamaPemb) {
        int jmlWarga = Integer.parseInt(rpjmJmlWarga);
        int biaya = Integer.parseInt(rpjmBiaya);
        int lamaPemb = Integer.parseInt(rpjmLamaPemb);

        if (jmlWarga > 3500) {
            return 3;
        } else if (jmlWarga <= 3500) {
            if (biaya <= 250000000) {
                return 2;
            } else if (biaya > 250000000){
                if (lamaPemb < 1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        }
        return 0;
    }
}
